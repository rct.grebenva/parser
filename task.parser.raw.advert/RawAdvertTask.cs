﻿using common.infrastructure;
using common.shared;
using integration.sql.context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using unity.infrastructure;
using Unity;

namespace task.parser.raw.advert
{
    public class RawAdvertTask
    {
        [Dependency]
        public RealestateContext RealestateContext { get; set; }

        [Dependency]
        public IFtpOperation FtpOperation { get; set; }

        public async Task Start()
        {
            var sourceParsers = new Dictionary<string, IRawAdvertParser>();
            foreach (var header in RealestateContext.AdvertHeader.ToList())
            {
                if (!header.IsModified) continue;

                var sourceName = ((Sources)header.Source).ToString().ToLower();
                if (!sourceParsers.ContainsKey(sourceName))
                {
                    if (Bootstrapper.Container.IsRegistered<IRawAdvertParser>(sourceName))
                        sourceParsers.Add(sourceName, Bootstrapper.Container.Resolve<IRawAdvertParser>(sourceName));
                    else
                        continue;
                }


                if (!sourceParsers[sourceName].TryParse(header))
                {
                    Console.WriteLine($"Raw Advert ({header.Url}) is NOT parsed successfull");
                }
                header.IsModified = false;
                header.ProcessedDate = DateTime.Now;
                await RealestateContext.SaveChangesAsync();
                Console.WriteLine($"Raw Advert ({header.Url}) is parsed successfull");
            }
        }
    }
}
