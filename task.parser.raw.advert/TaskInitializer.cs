﻿using common.shared;
using System;
using Unity;
using unity.eventbrokerextension;
using System.Threading.Tasks;
using task.timer;

namespace task.parser.raw.advert 
{
    public class TaskInitializer
    {
        [Dependency]
        public RawAdvertTask RawAdvertTask { get; set; }

        [InjectionConstructor]
        public TaskInitializer()
        {

        }

        [SubscribesTo(EventName.OnStartService)]
        public async Task StartService(object sender, EventArgs eventArgs)
        {
            TimerOperation.AddTask(eventArgs, RawAdvertTask.GetType(), RawAdvertTask.Start);
        }


    }
}
