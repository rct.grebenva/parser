﻿using common.infrastructure;
using integration.sql.context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using common.helper;
using Unity;
using System.Linq;
using OpenQA.Selenium.Remote;
using HtmlAgilityPack;
using System.Text.RegularExpressions;

namespace task.parser.raw.advert
{
    public abstract class RawAdvertParser : IRawAdvertParser
    {
        [Dependency]
        public RealestateContext RealestateContext { get; set; }

        [Dependency]
        public IBrowserOperation BrowserOperation { get; set; }
        public bool TryParse(AdvertHeader advertHeader)
        {
            return ((Func<bool>)(() => Parse(advertHeader))).Repeat<Exception, bool>(e => Console.WriteLine(e));
        }

        private bool Parse(AdvertHeader advertHeader)
        {
            HtmlDocument htmlDocument = null;
            if(!TryLoadAdvert(advertHeader.Url, out htmlDocument))
            {
                advertHeader.IsAvailable = false;
                RealestateContext.SaveChanges();
                return false;
            }

            try
            {
                var rawAdvert = new RawAdvert
                {
                    ParsedDate = DateTime.Now,
                    HeaderId = advertHeader.Id,
                    PuplishDate = GetPublishDate(htmlDocument),
                    Html = Regex.Replace(htmlDocument.DocumentNode.InnerHtml, @"\s{2,333}", ""),
                    Price = GetPrice(advertHeader, htmlDocument),
                    Address = GetAdress(advertHeader, htmlDocument)
                };
                return Save(rawAdvert);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        protected abstract string GetAdress(AdvertHeader advertHeader, HtmlDocument htmlDocument);
        protected abstract string GetPrice(AdvertHeader advertHeader, HtmlDocument htmlDocument);

        protected abstract DateTime GetPublishDate(HtmlDocument htmlDocument);

        protected abstract bool TryLoadAdvert(string url, out HtmlDocument htmlDocument);

        private bool Save(RawAdvert rawAdvert)
        {
            var existRawAdvert = RealestateContext
                .RawAdvert
                .FirstOrDefault(advert => advert.HeaderId == rawAdvert.HeaderId);
            var header = RealestateContext.AdvertHeader.Find(rawAdvert.HeaderId);
            rawAdvert.Header = header;

            if (existRawAdvert == null)
            {
                existRawAdvert = rawAdvert;
                RealestateContext.RawAdvert.Add(existRawAdvert);
            }

            existRawAdvert.ParsedDate = DateTime.Now;
            header.IsModified = false;
            RealestateContext.SaveChanges();
            return true;
        }
    }
}
