﻿using common.infrastructure;
using HtmlAgilityPack;
using integration.sql.context;
using System;
using System.Globalization;
using System.Linq;
using unity.core;

namespace task.parser.raw.advert
{
    [WithName("neagent")]
    public class NeagentRawAdvertParser : RawAdvertParser, IRawAdvertParser
    {
        protected override string GetAdress(AdvertHeader advertHeader, HtmlDocument htmlDocument)
        {
            return advertHeader.Tag1;
        }

        protected override string GetPrice(AdvertHeader advertHeader, HtmlDocument htmlDocument)
        {
            return advertHeader.Tag2;
        }

        protected override DateTime GetPublishDate(HtmlDocument htmlDocument)
        {
            var rawTime = htmlDocument.DocumentNode
                .SelectNodes("//div[@class='value text-right']")
                .FirstOrDefault(div => div.GetAttributeValue("data-jss", null) == "time-to-be-parsed")
                .InnerText.Trim();
            var publishDate = DateTime.ParseExact(rawTime, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
            return publishDate;
        }

        protected override bool TryLoadAdvert(string url, out HtmlDocument htmlDocument)
        {
            htmlDocument = null;
            try
            {
                htmlDocument = BrowserOperation.GetHtml(url, "//div[@class='center-wrapper']//div[@class='gcontainer']");
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
    }
}
