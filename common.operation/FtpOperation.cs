﻿using common.infrastructure;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace common.operation
{
    public class FtpOperation : IFtpOperation
    {
        private readonly string _host = System.Configuration.ConfigurationManager.AppSettings["ftp.host"];
        private readonly string _userName = System.Configuration.ConfigurationManager.AppSettings["ftp.user"];
        private readonly string _password = System.Configuration.ConfigurationManager.AppSettings["ftp.password"];
        public bool CreateDir(string path)
        {
            bool IsCreated = true;
            try
            {
                WebRequest request = WebRequest.Create(_host + path);
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                request.Credentials = new NetworkCredential(_userName, _password);
                using (var resp = (FtpWebResponse)request.GetResponse())
                {
                    Console.WriteLine(resp.StatusCode);
                }
            }
            catch (Exception ex)
            {
                IsCreated = false;
                Console.WriteLine(ex);
            }
            return IsCreated;
        }

        public bool SaveFile(string path, string content)
        {
            return SaveFile(path, Encoding.ASCII.GetBytes(content));
        }
        public bool SaveFile(string path, byte[] buffer)
        {
            bool IsCreated = true;
            try
            {
                var request = WebRequest.Create(new Uri(_host + path)) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UseBinary = true;
                request.UsePassive = true;
                request.KeepAlive = true;
                request.Credentials = new NetworkCredential(_userName, _password);

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(buffer, 0, buffer.Length);
                requestStream.Flush();
                requestStream.Close(); 
            }
            catch (Exception ex)
            {
                IsCreated = false;
                Console.WriteLine(ex);
            }
            return IsCreated;
        }
    }
}
