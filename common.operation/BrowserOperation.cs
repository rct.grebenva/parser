﻿using common.infrastructure;
using HtmlAgilityPack;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using System.Configuration;
using System.Threading;

namespace common.operation
{
    public class BrowserOperation : IBrowserOperation
    {
        private readonly string host = ConfigurationManager.AppSettings["selenium.host"];
        public HtmlDocument GetHtml(string url, string xpath = "/html", int sleepTime = 0)
        {
            var driver = new RemoteWebDriver(new Uri(host), new ChromeOptions());
            driver.Navigate().GoToUrl(url);
            Thread.Sleep(sleepTime);
            var html = driver.FindElementByXPath(xpath).GetAttribute("innerHTML");
            driver.Close();
            var page = new HtmlDocument();
            page.LoadHtml(html);
            return page;

        }
    }
}
