﻿using System.ComponentModel;

namespace common.shared
{
    public enum Sources
    {
        [Description("realt")]
        Realt,
        [Description("neagent")]
        Neagent,
        [Description("onliner")]
        Onliner
    }
}
