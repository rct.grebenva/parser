﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace common.shared
{
    public class ParsedAdvert
    {
        public string Url { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public DateTime Date { get; set; }
        public string ContractType { get; set; }
        public string RealtyType { get; set; }
        public double Price { get; set; }
        public int Rooms { get; set; }
        public string Currency { get; set; }
        public Agent Agent { get; set; }
        public double Square { get; set; }
        public double LivingSquare { get; set; }
        [JsonIgnore]
        private double? _kitchenSq = 0;
        public double? KitchenSquare { get { return _kitchenSq > 0 ? _kitchenSq : null; } set { _kitchenSq = value; } }
        public string Address { get; set; }
        public string Description { get; set; }
        public decimal Lat { get; set; }
        public decimal Long { get; set; }
        public int Floor { get; set; }
        public IEnumerable<string> Images { get; set; }
    }

    public class Agent
    {
        public string Name { get; set; }
        public bool IsAgent { get; set; }
        public IEnumerable<string> Phones { get; set; }
        public string Email { get; set; }
    }
}
