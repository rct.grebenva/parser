﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace common.shared
{
    public class WebHelper
    {
        public string GetHtml(string urlAddress, bool withScripts = false)
        {
            if (withScripts)
            {
                var host = ConfigurationManager.AppSettings["selenium.host"];
                using (var driver = new RemoteWebDriver(new Uri(host), new ChromeOptions()))
                {
                    driver.Navigate().GoToUrl(urlAddress);
                    Thread.Sleep(2000);
                    return driver.FindElementByTagName("html").GetAttribute("innerHTML");
                }
            }

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlAddress);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = null;

                if (response.CharacterSet == null)
                {
                    readStream = new StreamReader(receiveStream);
                }
                else
                {
                    readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
                }

                string data = readStream.ReadToEnd();

                response.Close();
                readStream.Close();
                return data;
            }
            return null;
        }

    }
}
