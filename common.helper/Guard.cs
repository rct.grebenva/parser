﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace common.helper
{
    public static class Guard
    {
        private const string guardRepeatTimesName = "guard.repeattimes";
        private static int guardRepeatTimes;

        static Guard()
        {
            if (!int.TryParse(System.Configuration.ConfigurationManager.AppSettings[guardRepeatTimesName], out guardRepeatTimes)) guardRepeatTimes = 3;
        }

        public static void LockAction<TAction>(this TAction entity, Action<TAction> action) where TAction : class
        {
            if (null == entity) action(entity);
            else
                lock (entity)
                {
                    action(entity);
                }
        }

        public static TResult LockAction<TAction, TResult>(this TAction entity, Func<TAction, TResult> function) where TAction : class
        {
            if (null == entity) return function(entity);
            else
                lock (entity)
                {
                    return function(entity);
                }
        }
        public static void Repeat<TException>(this Action action, Action<TException> callback = null) where TException : Exception
        {
            Repeat(() => { action(); return true; }, callback);
        }


        public static TOut Repeat<TException, TOut>(this Func<TOut> func, Action<TException> callback = null) where TException : Exception
        {
            TException exception;
            int repeatCount = guardRepeatTimes;
            do
            {
                try
                {
                    return func();
                }
                catch (TException ex)
                {
                    exception = ex;
                }
                //Thread.Sleep(3000);
            } while (--repeatCount > 0);

            callback?.Invoke(exception);
            throw exception;
        }
    }
}
