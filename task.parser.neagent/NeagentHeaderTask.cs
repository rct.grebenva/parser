﻿using common.helper.integration.sql.context;
using common.shared;
using HtmlAgilityPack;
using integration.sql.context;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Unity;

namespace task.parser.neagent
{
    public class NeagentHeaderTask : HeaderParser
    {
        [Dependency]
        public RentAdvertCaster RentAdvertCaster { get; set; }

        protected override Sources Source => Sources.Neagent;

        public async Task Parse()
        {
            Parse(
                @"https://neagent.by",
                ContractTypes.Rent,
                PropertyType.Flat);
        }

        protected override AdvertHeader GetHeaderFromDiv(HtmlNode htmlNode)
        {
            var header = new AdvertHeader()
            {
                CreatedDate = DateTime.Now,
                Url = htmlNode.SelectSingleNode("//a[@class='a_more ']").Attributes["href"].Value,
                Tag1 = htmlNode.SelectSingleNode("//div[@class='md_head']/em").InnerText,
                Tag2 = Regex.Replace(htmlNode.SelectSingleNode("//div[@class='itm_price']").InnerText, @"\s+", "")
            };

            return header;
        }

        protected override string GetNextUrl(RemoteWebDriver driver) => 
            driver?.FindElementsByLinkText(">")?.SingleOrDefault(element => element.GetAttribute("rel").Equals("next"))?.GetAttribute("href");

        protected override IEnumerable<IWebElement> GetDivsFromElement(RemoteWebDriver driver) => driver.FindElementsByClassName("imd");

        protected override string GetSummaryText(HtmlDocument node, string url)
        {
            return Regex.Replace(node.DocumentNode.InnerHtml, @"\s+", "");
        }
    }
}
