﻿using common.helper.integration.sql.context;
using common.shared;
using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;

namespace task.parser.gohome
{
    public class AdvertTask : HeaderParser
    {
        [Dependency]
        public Caster GohomeCaster { get; set; }

        protected override Sources Source => throw new System.NotImplementedException();

        public async Task Start(ContractTypes contractType, PropertyType realtyType, string siteUrl)
        {
            Parse(siteUrl,
                contractType,
                realtyType);
        }

        protected override IEnumerable<IWebElement> GetDivsFromElement(RemoteWebDriver driver) =>
            driver.FindElementsByXPath("//table[contains(@class, 'table-adveadverts-list')]//tbody//tr[contains(@class,'table-adveadverts-list')]");

        protected override string GetNextUrl(RemoteWebDriver driver) =>
            driver.FindElementByXPath("//div[@class='page_links']//span[@class='active']/following-sibling::span/a").GetAttribute("href");

        protected override AdvertHeader GetHeaderFromDiv(HtmlNode htmlNode) => new AdvertHeader();

        protected override string GetSummaryText(HtmlDocument node, string url)
        {
            throw new System.NotImplementedException();
        }
        //$"https://gohome.by{htmlNode.SelectSingleNode(".//td[@class='td-name']//div[@class='w-name']//a").Attributes["href"].Value}";
    }
}
