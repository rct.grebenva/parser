﻿using common.shared;
using System;
using Unity;
using unity.eventbrokerextension;
using System.Threading.Tasks;
using task.timer;

namespace task.parser.gohome
{
    public class TaskInitializer
    {
        [Dependency]
        public AdvertTask AdvertTask { get; set; }
        
        [InjectionConstructor]
        public TaskInitializer()
        {

        }

        [SubscribesTo(EventName.OnStartService)]
        public async Task StartService(object sender, EventArgs eventArgs)
        {
            TimerOperation.AddTask(eventArgs,
                AdvertTask.GetType(), 
                () => AdvertTask.Start(ContractTypes.Sale, PropertyType.Flat, @"https://gohome.by/sale/flat/minsk"),
                @"https://gohome.by/sale/flat/minsk");

            TimerOperation.AddTask(eventArgs,
                AdvertTask.GetType(),
                () => AdvertTask.Start(ContractTypes.Rent, PropertyType.Flat, @"https://gohome.by/rent/flat/minsk"),
                @"https://gohome.by/rent/flat/minsk");
        }


    }
}
