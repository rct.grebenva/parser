﻿using System;
using System.Collections.Generic;

namespace integration.sql.context
{
    public partial class Rigion
    {
        public Rigion()
        {
            City = new HashSet<City>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }

        public virtual Country Country { get; set; }
        public virtual ICollection<City> City { get; set; }
    }
}
