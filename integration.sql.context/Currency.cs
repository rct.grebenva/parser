﻿using System;
using System.Collections.Generic;

namespace integration.sql.context
{
    public partial class Currency
    {
        public Currency()
        {
            Advert = new HashSet<Advert>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        public virtual ICollection<Advert> Advert { get; set; }
    }
}
