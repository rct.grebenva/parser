﻿using System;
using System.Collections.Generic;

namespace integration.sql.context
{
    public partial class AdvertHeader
    {
        public AdvertHeader()
        {
            RawAdvert = new HashSet<RawAdvert>();
        }

        public int Id { get; set; }
        public string Url { get; set; }
        public DateTime? ProcessedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Source { get; set; }
        public bool IsModified { get; set; }
        public string SummaryHash { get; set; }
        public int ContractType { get; set; }
        public int PropertyType { get; set; }
        public string Tag1 { get; set; }
        public string Tag2 { get; set; }
        public bool IsAvailable { get; set; }

        public virtual ICollection<RawAdvert> RawAdvert { get; set; }
    }
}
