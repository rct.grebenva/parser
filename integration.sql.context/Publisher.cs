﻿using System;
using System.Collections.Generic;

namespace integration.sql.context
{
    public partial class Publisher
    {
        public Publisher()
        {
            Advert = new HashSet<Advert>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public int? AgencyId { get; set; }

        public virtual ICollection<Advert> Advert { get; set; }
    }
}
