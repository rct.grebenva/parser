﻿using System;
using System.Collections.Generic;

namespace integration.sql.context
{
    public partial class City
    {
        public City()
        {
            Street = new HashSet<Street>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int RegionId { get; set; }

        public virtual Rigion Region { get; set; }
        public virtual ICollection<Street> Street { get; set; }
    }
}
