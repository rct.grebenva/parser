﻿using System;
using System.Collections.Generic;

namespace integration.sql.context
{
    public partial class Advert
    {
        public Advert()
        {
            AdvertImage = new HashSet<AdvertImage>();
        }

        public int Id { get; set; }
        public int RawAdvertId { get; set; }
        public int BuildingId { get; set; }
        public double Price { get; set; }
        public int CurrencyId { get; set; }
        public int PublisherId { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Description { get; set; }
        public int RoomsCount { get; set; }
        public int? Floor { get; set; }
        public double? LivingSquare { get; set; }
        public double? Square { get; set; }
        public double? KitchenSquare { get; set; }
        public int Version { get; set; }

        public virtual Building Building { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual Publisher Publisher { get; set; }
        public virtual RawAdvert RawAdvert { get; set; }
        public virtual ICollection<AdvertImage> AdvertImage { get; set; }
    }
}
