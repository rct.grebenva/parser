﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace integration.sql.context
{
    public partial class RealestateContext : DbContext
    {
        public RealestateContext()
        {
        }

        public RealestateContext(DbContextOptions<RealestateContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Advert> Advert { get; set; }
        public virtual DbSet<AdvertHeader> AdvertHeader { get; set; }
        public virtual DbSet<AdvertImage> AdvertImage { get; set; }
        public virtual DbSet<Building> Building { get; set; }
        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<Coordinates> Coordinates { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Currency> Currency { get; set; }
        public virtual DbSet<Publisher> Publisher { get; set; }
        public virtual DbSet<RawAdvert> RawAdvert { get; set; }
        public virtual DbSet<Rigion> Rigion { get; set; }
        public virtual DbSet<Street> Street { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Advert>(entity =>
            {
                entity.HasIndex(e => e.BuildingId);

                entity.HasIndex(e => e.CurrencyId);

                entity.HasIndex(e => e.PublisherId);

                entity.HasIndex(e => e.RawAdvertId);

                entity.Property(e => e.Description).IsRequired();

                entity.Property(e => e.ModifyDate).HasColumnType("date");

                entity.HasOne(d => d.Building)
                    .WithMany(p => p.Advert)
                    .HasForeignKey(d => d.BuildingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Advert_Building");

                entity.HasOne(d => d.Currency)
                    .WithMany(p => p.Advert)
                    .HasForeignKey(d => d.CurrencyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Advert_Currency");

                entity.HasOne(d => d.Publisher)
                    .WithMany(p => p.Advert)
                    .HasForeignKey(d => d.PublisherId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Advert_Publisher");

                entity.HasOne(d => d.RawAdvert)
                    .WithMany(p => p.Advert)
                    .HasForeignKey(d => d.RawAdvertId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Advert_RawAdvert");
            });

            modelBuilder.Entity<AdvertHeader>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("date");

                entity.Property(e => e.ProcessedDate).HasColumnType("date");

                entity.Property(e => e.Tag1).HasMaxLength(250);

                entity.Property(e => e.Tag2).HasMaxLength(250);

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<AdvertImage>(entity =>
            {
                entity.HasKey(e => e.Guid);

                entity.Property(e => e.Guid)
                    .HasColumnName("GUID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Format)
                    .IsRequired()
                    .HasMaxLength(64);

                entity.HasOne(d => d.Advert)
                    .WithMany(p => p.AdvertImage)
                    .HasForeignKey(d => d.AdvertId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdvertImage_Advert");
            });

            modelBuilder.Entity<Building>(entity =>
            {
                entity.HasIndex(e => e.CoordinatesId);

                entity.HasIndex(e => e.StreetId);

                entity.Property(e => e.Block).HasMaxLength(4);

                entity.HasOne(d => d.Coordinates)
                    .WithMany(p => p.Building)
                    .HasForeignKey(d => d.CoordinatesId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Building_Coordinates");

                entity.HasOne(d => d.Street)
                    .WithMany(p => p.Building)
                    .HasForeignKey(d => d.StreetId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Building_Street");
            });

            modelBuilder.Entity<City>(entity =>
            {
                entity.HasIndex(e => e.RegionId);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.City)
                    .HasForeignKey(d => d.RegionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_City_Rigion");
            });

            modelBuilder.Entity<Coordinates>(entity =>
            {
                entity.Property(e => e.Lat).HasColumnType("decimal(12, 9)");

                entity.Property(e => e.Long).HasColumnType("decimal(12, 9)");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.ShortName).HasMaxLength(2);
            });

            modelBuilder.Entity<Currency>(entity =>
            {
                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(8);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(128);
            });

            modelBuilder.Entity<Publisher>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<RawAdvert>(entity =>
            {
                entity.HasIndex(e => e.HeaderId);

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Html).IsRequired();

                entity.Property(e => e.ParsedDate).HasColumnType("date");

                entity.Property(e => e.Price)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.PuplishDate).HasColumnType("date");

                entity.HasOne(d => d.Header)
                    .WithMany(p => p.RawAdvert)
                    .HasForeignKey(d => d.HeaderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RawAdvert_AdvertHeader");
            });

            modelBuilder.Entity<Rigion>(entity =>
            {
                entity.HasIndex(e => e.CountryId);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Rigion)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Rigion_Country");
            });

            modelBuilder.Entity<Street>(entity =>
            {
                entity.HasIndex(e => e.CityId);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Street)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Street_City");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
