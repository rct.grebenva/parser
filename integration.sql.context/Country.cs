﻿using System;
using System.Collections.Generic;

namespace integration.sql.context
{
    public partial class Country
    {
        public Country()
        {
            Rigion = new HashSet<Rigion>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public int? CountryCode { get; set; }

        public virtual ICollection<Rigion> Rigion { get; set; }
    }
}
