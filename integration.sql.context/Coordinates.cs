﻿using System;
using System.Collections.Generic;

namespace integration.sql.context
{
    public partial class Coordinates
    {
        public Coordinates()
        {
            Building = new HashSet<Building>();
        }

        public int Id { get; set; }
        public decimal Lat { get; set; }
        public decimal Long { get; set; }

        public virtual ICollection<Building> Building { get; set; }
    }
}
