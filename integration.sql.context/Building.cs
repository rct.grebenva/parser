﻿using System;
using System.Collections.Generic;

namespace integration.sql.context
{
    public partial class Building
    {
        public Building()
        {
            Advert = new HashSet<Advert>();
        }

        public int Id { get; set; }
        public int Number { get; set; }
        public string Block { get; set; }
        public int? Zipcode { get; set; }
        public int? Floors { get; set; }
        public int? Year { get; set; }
        public int StreetId { get; set; }
        public int CoordinatesId { get; set; }

        public virtual Coordinates Coordinates { get; set; }
        public virtual Street Street { get; set; }
        public virtual ICollection<Advert> Advert { get; set; }
    }
}
