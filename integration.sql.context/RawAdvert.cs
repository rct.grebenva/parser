﻿using System;
using System.Collections.Generic;

namespace integration.sql.context
{
    public partial class RawAdvert
    {
        public RawAdvert()
        {
            Advert = new HashSet<Advert>();
        }

        public int Id { get; set; }
        public int HeaderId { get; set; }
        public string Address { get; set; }
        public string Price { get; set; }
        public DateTime ParsedDate { get; set; }
        public DateTime PuplishDate { get; set; }
        public string Html { get; set; }

        public virtual AdvertHeader Header { get; set; }
        public virtual ICollection<Advert> Advert { get; set; }
    }
}
