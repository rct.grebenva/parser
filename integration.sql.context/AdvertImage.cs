﻿using System;
using System.Collections.Generic;

namespace integration.sql.context
{
    public partial class AdvertImage
    {
        public Guid Guid { get; set; }
        public int AdvertId { get; set; }
        public string Format { get; set; }

        public virtual Advert Advert { get; set; }
    }
}
