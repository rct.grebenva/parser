﻿using System;
using System.Collections.Generic;

namespace integration.sql.context
{
    public partial class Street
    {
        public Street()
        {
            Building = new HashSet<Building>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int CityId { get; set; }

        public virtual City City { get; set; }
        public virtual ICollection<Building> Building { get; set; }
    }
}
