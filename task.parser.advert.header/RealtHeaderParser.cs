﻿using integration.sql.context;
using common.shared;
using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using unity.core;
using common.infrastructure;

namespace task.parser.advert.header
{
    [WithName("realt")]
    public class RealtHeaderParser : HeaderParser, IHeaderParser
    {

        protected override Sources Source => Sources.Realt;

        protected override IEnumerable<IWebElement> GetDivsFromElement(RemoteWebDriver driver)
        {
            IEnumerable<IWebElement> result = null;
            try
            {
                var test = driver.FindElementByTagName("html").GetAttribute("innerHTML");
                result = driver.FindElementsByClassName("bd-table-item");
                if (result.Any()) return result;
                return driver.FindElementsByXPath("//div[@class='bd-table-item vip-item']");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return result;
            }
        }


        protected override string GetNextUrl(RemoteWebDriver driver)
        {
            var uniPaging = driver.FindElementByClassName("uni-paging");
            var active = uniPaging.FindElement(By.ClassName("active"));
            var nextSibling = active.FindElement(By.XPath("./following-sibling::a"));
            return $"{nextSibling.GetAttribute("href")}&view=0";
        }

        protected override AdvertHeader GetHeaderFromDiv(HtmlNode htmlNode)
        {
            var header = new AdvertHeader()
            {
                CreatedDate = DateTime.Now,
                Url = htmlNode.SelectSingleNode("//div[@class='ad']/a").Attributes["href"].Value
            };

            return header;
        }
        protected override string GetSummaryText(HtmlDocument node, string url)
        {
            return Regex.Replace(node.DocumentNode.InnerHtml, @"\s+", "");
        }
    }
}
