﻿using common.shared;
using System;
using Unity;
using unity.eventbrokerextension;
using System.Threading.Tasks;
using task.timer;
using System.Linq;
using common.infrastructure;
using unity.infrastructure;

namespace task.parser.advert.header
{
    public class TaskInitializer
    {

        [InjectionConstructor]
        public TaskInitializer()
        {

        }

        [SubscribesTo(EventName.OnStartService)]
        public async Task StartService(object sender, EventArgs eventArgs)
        {
            var neagent = Bootstrapper.Container.Resolve<IHeaderParser>(Sources.Neagent.ToString().ToLower());
            TimerOperation.AddTask(eventArgs, neagent.GetType(), () => neagent.TryParse(@"https://neagent.by", ContractTypes.Rent, PropertyType.Flat));


            var realt = Bootstrapper.Container.Resolve<IHeaderParser>(Sources.Realt.ToString().ToLower());
            TimerOperation.AddTask(eventArgs,
                realt.GetType(),
                () => realt.TryParse(@"https://realt.by/sale/flats/?view=0#tabs",ContractTypes.Sale, PropertyType.Flat),
                @"https://realt.by/sale/flats/?view=0#tabs");

            TimerOperation.AddTask(eventArgs,
                realt.GetType(),
                () => realt.TryParse(@"https://realt.by/rent/flat-for-long/?view=0#tabs", ContractTypes.Rent, PropertyType.Flat),
                @"https://realt.by/rent/flat-for-long/?view=0#tabs");
        }


    }
}
