﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Unity;
using System.Security.Cryptography;
using HtmlAgilityPack;
using System.Threading;
using integration.sql.context;
using common.shared;
using common.helper;
using System.Threading.Tasks;

namespace task.parser.advert.header
{
    public abstract class HeaderParser
    {
        [Dependency]
        public RealestateContext RealestateContext { get; set; }

        protected abstract Sources Source { get; }

        [Dependency]
        public WebHelper WebHelper { get; set; }

        public Task TryParse(
               string siteUrl,
               ContractTypes contractType,
               PropertyType realtyType)
        {

            (new Action(() => Parse(siteUrl, contractType, realtyType))).Repeat<Exception>(e => Console.WriteLine(e));
            return Task.FromResult(0);
        }
        private void Parse
               (
               string siteUrl,
               ContractTypes contractType,
               PropertyType realtyType)
        {
            Console.OutputEncoding = Encoding.UTF8;
            var host = ConfigurationManager.AppSettings["selenium.host"];
            RemoteWebDriver driver = null;
            {
                try
                {
                    string nextUrl = siteUrl;
                    do
                    {
                        driver = new RemoteWebDriver(new Uri(host), new ChromeOptions());
                        driver.Navigate().GoToUrl(nextUrl);
                        Thread.Sleep(5000);
                        var divs = GetDivsFromElement(driver)
                            .Select(div =>
                            {
                                var form = new HtmlAgilityPack.HtmlDocument();
                                form.LoadHtml(div.GetAttribute("innerHTML"));
                                return form;
                            })
                            .Skip(1)
                            .ToList();
                        foreach (var formNode in divs)
                        {
                            try
                            {
                                var header = GetHeaderFromDiv(formNode.DocumentNode);
                                var summaryText = GetSummaryText(formNode, header.Url);
                                header.SummaryHash = GetAdvertSumamryHash(summaryText);
                                Save(header, realtyType, contractType);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }
                        }
                        nextUrl = GetNextUrl(driver);
                        driver.Close();
                    } while (!string.IsNullOrEmpty(nextUrl));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    driver.Close();
                }
            }
        }

        protected abstract string GetSummaryText(HtmlDocument node, string url);
        
        protected virtual string GetAdvertSumamryHash(string summaryText)
        {
            var sha256 = SHA256.Create();
            var data = sha256.ComputeHash(Encoding.UTF8.GetBytes(summaryText));
            var sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
        protected abstract AdvertHeader GetHeaderFromDiv(HtmlAgilityPack.HtmlNode htmlNode);

        protected abstract string GetNextUrl(RemoteWebDriver driver);

        protected abstract IEnumerable<IWebElement> GetDivsFromElement(RemoteWebDriver driver);
        
       
        private HtmlNode GetHtmlNode(string urlAddress, bool withJS = false)
        {
            var html = WebHelper.GetHtml(urlAddress, withJS);
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);
            return doc.DocumentNode;
        }

        public void Save(AdvertHeader advertHeader, PropertyType propertyType, ContractTypes contractType)
        {
            var newHeader = RealestateContext.AdvertHeader.FirstOrDefault(header => header.Url == advertHeader.Url);
            
            if(newHeader == null)
            {
                newHeader = advertHeader;
                newHeader.Source = (int)Source;
                newHeader.ContractType = (int)contractType;
                newHeader.PropertyType = (int)propertyType;
                
                RealestateContext.AdvertHeader.Add(newHeader);
            }
            newHeader.CreatedDate = DateTime.Now;
            newHeader.IsModified = !newHeader.IsModified ? newHeader.SummaryHash != advertHeader.SummaryHash : false;
            newHeader.SummaryHash = advertHeader.SummaryHash;
            newHeader.IsAvailable = true;

            RealestateContext.SaveChanges();
            Console.WriteLine($"Header saved {newHeader.Url}.");
        }
    }
}
