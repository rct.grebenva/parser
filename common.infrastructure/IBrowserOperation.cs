﻿using HtmlAgilityPack;

namespace common.infrastructure
{
    public interface IBrowserOperation
    {
        HtmlDocument GetHtml(string url, string xpath = "/html", int sleepTime = 0);
    }
}
