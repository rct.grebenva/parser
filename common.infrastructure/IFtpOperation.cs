﻿namespace common.infrastructure
{
    public interface IFtpOperation
    {
        bool SaveFile(string path, string content);
        bool SaveFile(string path, byte[] data);
        bool CreateDir(string path);
    }
}
