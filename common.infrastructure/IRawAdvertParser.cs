﻿using integration.sql.context;
using System.Threading.Tasks;

namespace common.infrastructure
{
    public interface IRawAdvertParser
    { 
        bool TryParse(AdvertHeader advertHeader);
    }
}
