﻿using common.shared;
using System.Threading.Tasks;

namespace common.infrastructure
{
    public interface IHeaderParser
    {
        Task TryParse(
               string siteUrl,
               ContractTypes contractType,
               PropertyType realtyType);
    } 
}
