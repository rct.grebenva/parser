﻿using common.helper.integration.sql.context;
using common.shared;
using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;

namespace task.parser.domovita
{
    public class AdvertTask : HeaderParser
    {
        [Dependency]
        public Caster DomovitaCaster { get; set; }

        protected override Sources Source => throw new System.NotImplementedException();

        public async Task Start(ContractTypes contractType, PropertyType realtyType, string siteUrl)
        {
            Parse(siteUrl,
                contractType,
                realtyType);
        }

        protected override IEnumerable<IWebElement> GetDivsFromElement(RemoteWebDriver driver) =>
            driver.FindElementsByXPath("//table[@class='table fs-14 table_view items']//tbody//tr");

        protected override string GetNextUrl(RemoteWebDriver driver) =>
            driver.FindElementByXPath("//a[text()='Следующая']").GetAttribute("href");

        protected override AdvertHeader GetHeaderFromDiv(HtmlNode htmlNode) =>
          new AdvertHeader(); //  htmlNode.SelectSingleNode(".//td[@class='address']//a").Attributes["href"].Value;

        protected override string GetSummaryText(HtmlDocument node, string url)
        {
            throw new System.NotImplementedException();
        }
    }
}
